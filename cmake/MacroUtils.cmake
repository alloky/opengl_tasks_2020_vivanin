macro(ADD_TASKS DIRNAME)
    file(GLOB STUDENTS ${DIRNAME}/*)
    foreach(STUDENT ${STUDENTS})
        if(IS_DIRECTORY ${STUDENT})
            message("Student task found " ${STUDENT})
            if(SELECTED_STUDENT)
                get_filename_component(STUDENT_NAME ${STUDENT} NAME)
                if(${SELECTED_STUDENT} STREQUAL ${STUDENT_NAME})
                    message("Add to build " ${STUDENT})
                    add_subdirectory("${STUDENT}")
                endif()
            else()
                add_subdirectory("${STUDENT}")
            endif()
        endif()
    endforeach()
endmacro(ADD_TASKS DIRNAME)

macro(MAKE_TASK TRGTNAME TASKNUM SOURCEFILES HEADERS)
    message("Building target ${TRGTNAME}${TASKNUM}")
    message("222" ${HEADERS})
    add_executable(${TRGTNAME}${TASKNUM} ${SOURCEFILES} ${HEADERS})

    target_include_directories(${TRGTNAME}${TASKNUM} PUBLIC
        ${GLEW_INCLUDE_DIR}
        ${GLM_INCLUDE_DIR}
        ${SOIL_INCLUDE_DIR}
        ${PROJECT_SOURCE_DIR}/common
    )

    target_link_libraries(${TRGTNAME}${TASKNUM}
        ${GLEW_LIBS}
        ${GLM_LIBS}
        ${SOIL_LIBS}
    )

    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        target_link_libraries(${TRGTNAME}${TASKNUM} "-framework CoreFoundation")
    endif()

    install(TARGETS ${TRGTNAME}${TASKNUM} RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/task${TASKNUM})

#    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${TRGTNAME}Data${TASKNUM})
#        add_custom_target(${TRGTNAME}${TASKNUM}_Data${TASKNUM} ${PROJECT_SOURCE_DIR}/
#                COMMAND ${CMAKE_COMMAND} -E remove_directory
#                ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TRGTNAME}Data${TASKNUM}
#                COMMAND ${CMAKE_COMMAND} -E copy_directory
#                ${CMAKE_CURRENT_SOURCE_DIR}/${TRGTNAME}Data${TASKNUM}
#                ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TRGTNAME}Data${TASKNUM}
#                COMMENT "Force copy resources to ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TRGTNAME}Data${TASKNUM}"
#                VERBATIM)
#
#        add_dependencies(${TRGTNAME}${TASKNUM} ${TRGTNAME}${TASKNUM}_Data${TASKNUM})
#    else()
#        message("Data folder not found " ${TRGTNAME}Data${TASKNUM})
#    endif()
endmacro()

macro(MAKE_OPENGL_TASK TRGTNAME TASKNUM SOURCEFILES HEADERS)
    MAKE_TASK(${TRGTNAME} ${TASKNUM} "${SOURCEFILES}" "${HEADERS}")

#    message("333" ${PROJECT_SOURCE_DIR}/common)
    target_include_directories(${TRGTNAME}${TASKNUM} PUBLIC
            ${ASSIMP_INCLUDE_DIR}
            ${GLFW_INCLUDE_DIR}
            ${IMGUI_INCLUDE_DIR}
    )

    target_link_libraries(${TRGTNAME}${TASKNUM}
            ${ASSIMP_LIBS}
            ${GLFW_LIBS}
            ${IMGUI_LIBS}
    )

    if(USE_CORE_PROFILE)
        target_compile_definitions(${TRGTNAME}${TASKNUM} PRIVATE USE_CORE_PROFILE)
    endif(USE_CORE_PROFILE)
endmacro()


macro(MAKE_SAMPLE TRGTNAME)
    # For VS:
    source_group("Shaders" FILES ${SHADER_FILES})

    add_executable(${TRGTNAME} ${TRGTNAME}.cpp ${SRC_FILES} ${HEADER_FILES} ${SHADER_FILES})

    target_include_directories(${TRGTNAME} PUBLIC
            ${PROJECT_SOURCE_DIR}/common
            ${ASSIMP_INCLUDE_DIR}
            ${GLEW_INCLUDE_DIR}
            ${GLFW_INCLUDE_DIR}
            ${GLM_INCLUDE_DIR}
            ${IMGUI_INCLUDE_DIR}
            ${SOIL_INCLUDE_DIR}
            )

    target_link_libraries(${TRGTNAME}
            ${ASSIMP_LIBS}
            ${GLEW_LIBS}
            ${GLFW_LIBS}
            ${IMGUI_LIBS}
            ${SOIL_LIBS}
            )

    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        target_link_libraries(${TRGTNAME} "-framework CoreFoundation")
    endif()

    install(TARGETS ${TRGTNAME} RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})

    # Copy common resources

    # Copy models
    add_custom_command(TARGET ${TRGTNAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${CMAKE_SOURCE_DIR}/models
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/models VERBATIM)

    # Copy textures
    add_custom_command(TARGET ${TRGTNAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${CMAKE_SOURCE_DIR}/images
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/images VERBATIM)

    # Copy shaders
#    add_custom_command(TARGET ${TRGTNAME} POST_BUILD
#            COMMAND ${CMAKE_COMMAND} -E copy_directory
#            ${CMAKE_SOURCE_DIR}/shaders
#            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/shaders VERBATIM)
endmacro(MAKE_SAMPLE TRGTNAME)

# For copying local resources
macro(COPY_RESOURCE RESOURCEDIR)
    add_custom_target(${RESOURCEDIR}
            COMMAND ${CMAKE_COMMAND} -E remove_directory
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${RESOURCEDIR}
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${CMAKE_CURRENT_SOURCE_DIR}/${RESOURCEDIR}
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${RESOURCEDIR}
            COMMENT "Force copy resources to ${CMAKE_CURRENT_BINARY_DIR}/${RESOURCEDIR}"
            VERBATIM)
endmacro()

# For copying local resources with change in path
macro(COPY_RESOURCE2 TRGTNAME SRCLOCAL DSTLOCAL)
    add_custom_command(TARGET ${TRGTNAME}
            COMMAND ${CMAKE_COMMAND} -E remove_directory
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${DSTLOCAL}
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${CMAKE_CURRENT_SOURCE_DIR}/${SRCLOCAL}
            ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${DSTLOCAL}
            COMMENT "Force copy resources ${SRCLOCAL} -> ${DSTLOCAL}"
            VERBATIM)
endmacro()