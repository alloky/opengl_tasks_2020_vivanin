/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330


struct MaterialInfo
{
    vec3 Ka;
    vec3 Kd;
    vec3 Ks;
    float s;
    bool haveNormalMap;
};

struct LightInfo {
    vec3 pos;
    vec3 La;
    vec3 Ld;
    vec3 Ls;
    float a0;
    float a1;
    float a2;
};


in vec4 color;
in vec3 normCam;
in vec4 posCam;
in vec2 texCoord;
in mat3 tbn;
in vec4 shadowTexCoord;

out vec4 fragColor;

uniform LightInfo light;
uniform MaterialInfo material;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;
uniform sampler2DShadow shadowTex; //Вариант 2


float d;
vec3 l;
vec3 h;

vec3 lightCoef;
vec3 ambient;
vec3 diffuse;
vec3 specular;

float division;

void main()
{
//    float bias = 0.0;
//    vec4 shadowSampleCoord = shadowTexCoord;
//    shadowSampleCoord.z += bias * shadowSampleCoord.w;
//    float visibility = textureProj(shadowTex, shadowSampleCoord); //глубина ближайшего фрагмента в пространстве источника света

    float visibility = 1.0; 

    vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
    vec3 normal;
    if (material.haveNormalMap) {
        vec3 normalFromTex = normalize(2*(texture(normalTex, texCoord).rgb-0.5));
        normal = normalize(tbn*normalFromTex);
    } else {
        normal = normalize(tbn*vec3(0,0,1));
    }

    vec3 viewDirection = normalize(-posCam.xyz); //направление на виртуальную камеру
    vec3 lightDirCamSpace = normalize(light.pos - posCam.xyz); //направление на источник света
    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
    vec3 lightDir = vec3(0, -0.25,-1);

    // модуль константного вектора lightDir
    float aLen = pow(lightDir.x, 2)+pow(lightDir.y,2)+pow(lightDir.z,2);
    // модуь вектора камеры lightDirCamSpace
    float bLen = pow(lightDirCamSpace.x, 2)+pow(lightDirCamSpace.y,2)+pow(lightDirCamSpace.z,2);
    float abDotSqr = pow(dot(lightDir, lightDirCamSpace), 2);
    float lenSqrProd = aLen*bLen;

    float intensity = 1.;

    // ограничиваем осевщаемую область по углу
    intensity = 1.5 * (clamp( abDotSqr / lenSqrProd, 0.75, 1.) - 0.75) / 0.25;

    float dd = pow(posCam.x-light.pos.x,2)+pow(posCam.y-light.pos.y,2)+pow(posCam.z-light.pos.z,2);
    float d = sqrt(dd);

    division = light.a0 + light.a1 * d + light.a2 * d * d;

    vec3 color = diffuseColor * (material.Ka * light.La + (material.Kd * light.Ld * NdotL  / division) * visibility) * intensity;

    if (NdotL > 0.0)
    {
        //биссектриса между направлениями на камеру и на источник света
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

        float blinnTerm = max(dot(normal, halfVector), 0.0);  //интенсивность бликового освещения по Блинну
        blinnTerm = pow(blinnTerm, material.s);  //регулируем размер блика
        color +=  material.Ks*light.Ls * blinnTerm / division * visibility;
    }

    fragColor = vec4(color, 1.0);

}
