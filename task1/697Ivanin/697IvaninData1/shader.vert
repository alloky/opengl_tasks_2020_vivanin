/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330


layout(std140) uniform Matrices
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
};

uniform mat4 modelMatrix;

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;

//стандартные матрицы для проектора
uniform mat4 lightViewMatrix; //из мировой в систему координат камеры
uniform mat4 lightProjectionMatrix; //из системы координат камеры в усеченные координаты
uniform mat4 lightScaleBiasMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec3 vertexTangent;
layout(location = 4) in vec3 vertexBitangent;

out vec4 color;
out vec3 normCam;
out vec4 posCam;
out vec2 texCoord; //текстурные координаты
out mat3 tbn;
out vec4 shadowTexCoord; //выходные текстурные координаты для проективное текстуры

void main()
{
    texCoord = vertexTexCoord;

    //вычисляем текстурные координаты для теневой карты
    shadowTexCoord = lightScaleBiasMatrix * lightProjectionMatrix * lightViewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

    color = vec4(0.5*vertexNormal.xyz + 0.5, 1.0);

    posCam = viewMatrix * modelMatrix * vec4(vertexPosition.xyz, 1.0); //преобразование координат вершины в систему координат камеры
    normCam = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры

    vec3 tangentCam = normalize(normalToCameraMatrix * vertexTangent);
    vec3 bitangentCam = normalize(normalToCameraMatrix * vertexBitangent);
    tbn = mat3(tangentCam, bitangentCam, normCam);

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}