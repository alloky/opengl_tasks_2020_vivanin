//
// Created by alloky on 06.03.2020.
//


//std::vector<float> read_points(){
//    std::string line;
//    std::ifstream infile("lab.txt");
//
//    std::vector<float> points;
//
//    while (std::getline(infile, line))  // this does the checking!
//    {
//        std::istringstream iss(line);
//
//        float x, y, z;
//        iss >> x >> y >> z;
//        points.push_back(x);
//        points.push_back(y);
//        points.push_back(z);
//    }
//
//    std::cout << "end reading";
//
//    return points;
//}


#include <sstream>
#include <fstream>
#include <string>
#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <vector>
#include <MatrerialInfo.hpp>
#include <Texture.hpp>
#include "mesh_loader.hpp"

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    MeshPtr _cube;
    MeshPtr _bunny;
    MeshPtr _labyrinth_walls;
    MeshPtr _labyrinth_floor;
    MeshPtr _labyrinth_high_walls;


    ShaderProgramPtr _shader;
    ShaderProgramPtr _renderToShadowMapShader;
    ShaderProgramPtr _commonWithShadowsShader;

    GLuint _framebufferId;
    GLuint _depthTexId;
    GLuint _depthSampler;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;

    LightInfo highPointLight = LightInfo();

    LightInfo headLight = LightInfo();

    MaterialInfo material;

    TexturePtr _floorTexture;
    TexturePtr _highFloorTexture;
    TexturePtr _wallTexture;
    TexturePtr _floorNormTexture;
    TexturePtr _wallNormTexture;

    GLuint _floorSampler;
    GLuint _wallSampler;
    GLuint _highFloorSampler;


    std::vector<TexturePtr> _placats_texts;
    std::vector<MeshPtr> _placats;
    MaterialInfo _placat_mat;


    void makeScene() override
    {
        Application::makeScene();

//        _cube = makeCube(0.5);
//        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
//
//        _bunny = loadFromFile("models/bunny.obj");
//        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));


        _cube = makeCube(0.1f);
        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, 1.5f, 2.0f)));

        _labyrinth_walls = load_mesh("models/labyrinth_walls.txt");
        _labyrinth_walls->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0, 1.0f)));

        _labyrinth_floor = load_mesh("models/labyrinth_floor.txt");
        _labyrinth_floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0, 1.0f)));

        _labyrinth_high_walls = load_mesh("models/labyrinth_high_walls.txt");
        _labyrinth_high_walls->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0, 1.0f)));

        _placats = std::vector<MeshPtr>(3);
        for (int i = 0; i < 3; i++) {
            _placats[i] = load_mesh("models/placat_1.txt");
            _placats[i]->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f + 2.0f*i, 0.0f, 0.0f)));
        }

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("697IvaninData1/shader.vert", "697IvaninData1/shader.frag");
        _renderToShadowMapShader = std::make_shared<ShaderProgram>("697IvaninData1/toshadow.vert", "697IvaninData1/toshadow.frag");


        //=========================================================
        //Загрузка и создание текстур
//        _floorTexture = loadTexture("697IvaninData1/floor.jpg");

        _wallTexture = loadTexture("images/wall.jpg");
        _wallNormTexture = loadTexture("images/wall_norm.jpg");
        _wallSampler = initSampler(_wallSampler);


        _floorTexture = loadTexture("images/tiles.jpg");
        _floorNormTexture = loadTexture("images/tiles_norm.jpg");
        _floorSampler = initSampler(_floorSampler);

        _highFloorTexture = loadTexture("images/floor.jpg");
        _highFloorSampler = initSampler(_floorSampler);


        _placats_texts = std::vector<TexturePtr>(3);
        for (int i = 0; i < 3; i++) {
            _placats_texts[i] = loadTexture("images/placat"+std::to_string(i+1)+".jpg");
        }


//        initSampler(_floorSampler);

//
//        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };
//
//        glGenSamplers(1, &_depthSampler);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
//        glSamplerParameterfv(_depthSampler, GL_TEXTURE_BORDER_COLOR, border);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
//        glSamplerParameteri(_depthSampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

        //=========================================================
        //Инициализация фреймбуфера для рендера теневой карты

//        initFramebuffer();

        //=========================================================
        //Инициализация Uniform Buffer Object

        // Выведем размер Uniform block'а.
        GLint uniformBlockDataSize;
        glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
        std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
            glGenBuffers(1, &_ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
        // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);


        // Получение информации обо всех uniform-переменных шейдерной программы.
        if (USE_INTERFACE_QUERY) {
            GLsizei uniformsCount;
            GLsizei maxNameLength;
            glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformsCount);
            glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_MAX_NAME_LENGTH, &maxNameLength);
            std::vector<char> nameBuffer(maxNameLength);

            std::vector<GLenum> properties = {GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX};
            enum Property {
                Type,
                ArraySize,
                Offset,
                BlockIndex
            };
            for (GLuint uniformIndex = 0; uniformIndex < uniformsCount; uniformIndex++) {
                std::vector<GLint> params(properties.size());
                glGetProgramResourceiv(_shader->id(), GL_UNIFORM, uniformIndex, properties.size(), properties.data(),
                                       params.size(), nullptr, params.data());
                GLsizei realNameLength;
                glGetProgramResourceName(_shader->id(), GL_UNIFORM, uniformIndex, maxNameLength, &realNameLength, nameBuffer.data());

                std::string uniformName = std::string(nameBuffer.data(), realNameLength);

                std::cout << "Uniform " << "index = " << uniformIndex << ", name = " << uniformName << ", block = " << params[BlockIndex] << ", offset = " << params[Offset] << ", array size = " << params[ArraySize] << ", type = " << params[Type] << std::endl;
            }
        }
        else {
            GLsizei uniformsCount;
            glGetProgramiv(_shader->id(), GL_ACTIVE_UNIFORMS, &uniformsCount);

            std::vector<GLuint> uniformIndices(uniformsCount);
            for (int i = 0; i < uniformsCount; i++)
                uniformIndices[i] = i;
            std::vector<GLint> uniformBlocks(uniformsCount);
            std::vector<GLint> uniformNameLengths(uniformsCount);
            std::vector<GLint> uniformTypes(uniformsCount);
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_BLOCK_INDEX, uniformBlocks.data());
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_NAME_LENGTH, uniformNameLengths.data());
            glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_TYPE, uniformTypes.data());

            for (int i = 0; i < uniformsCount; i++) {
                std::vector<char> name(uniformNameLengths[i]);
                GLsizei writtenLength;
                glGetActiveUniformName(_shader->id(), uniformIndices[i], name.size(), &writtenLength, name.data());
                std::string uniformName = name.data();

                std::cout << "Uniform " << "index = " << uniformIndices[i] << ", name = " << uniformName << ", block = " << uniformBlocks[i] << ", type = " << uniformTypes[i] << std::endl;
            }
        }
    }


    void initFramebuffer()
    {
        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        //----------------------------

        //Создаем текстуру, куда будем впоследствии копировать буфер глубины
        glGenTextures(1, &_depthTexId);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, _fbWidth, _fbHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexId, 0);

        //----------------------------

        // Указываем, что для текущего фреймбуфера первый выход фрагментного шейдера никуда не пойдет.
        GLenum buffers[] = { GL_NONE };
        glDrawBuffers(1, buffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    GLuint initSampler(GLuint _sampler) const {
        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        return _sampler;
    }

    void update() override
    {
        Application::update();

        //Обновляем содержимое Uniform Buffer Object

#if 0
        //Вариант с glMapBuffer
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
        memcpy(p, &_camera, sizeof(_camera));
        glUnmapBuffer(GL_UNIFORM_BUFFER);
#elif 0
        //Вариант с glBufferSubData
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(_camera), &_camera);
#else
        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
                {
                        "viewMatrix",
                        "projectionMatrix"
                };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

        // Вывод оффсетов.
        static bool hasOutputOffset = false;
        if (!hasOutputOffset) {
            std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
            hasOutputOffset = true;
        }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
        if (USE_DSA) {
            glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }
        else {
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }

#endif
    }

    void drawToShadowMap(const CameraInfo& lightCamera, int width, int height)
    {
        //=========== Сначала подключаем фреймбуфер и рендерим в текстуру ==========
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        glViewport(0, 0, _fbWidth, _fbHeight);
        glClear(GL_DEPTH_BUFFER_BIT);

        _renderToShadowMapShader->use();
        _renderToShadowMapShader->setMat4Uniform("lightViewMatrix", _camera.viewMatrix);
        _renderToShadowMapShader->setMat4Uniform("lightProjectionMatrix", _camera.projMatrix);

//        if (true)
//        {
//            glEnable(GL_CULL_FACE);
//            glFrontFace(GL_CCW);
//            glCullFace(GL_FRONT);
//        }

        _renderToShadowMapShader->setMat4Uniform("modelMatrix", _labyrinth_walls->modelMatrix());
        _renderToShadowMapShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_walls->modelMatrix()))));

        _labyrinth_walls->draw();

        _renderToShadowMapShader->setMat4Uniform("modelMatrix", _labyrinth_high_walls->modelMatrix());
        _renderToShadowMapShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_high_walls->modelMatrix()))));


        _labyrinth_high_walls->draw();

        _renderToShadowMapShader->setMat4Uniform("modelMatrix", _labyrinth_floor->modelMatrix());
        _renderToShadowMapShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_floor->modelMatrix()))));

        _labyrinth_floor->draw();

//        if (true)
//        {
//            glDisable(GL_CULL_FACE);
//        }

        glUseProgram(0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0); //Отключаем фреймбуфер
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);


        glViewport(0, 0, width, height);

//        drawToShadowMap(_camera, width, height);


        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);


//        highPointLight.position = glm::vec3(50, 50, 50);
        highPointLight.position = _cameraMover->getPos() + glm::vec3(0, 0, 0.4);

        highPointLight.ambient =  glm::vec3(1, 1, 1) * glm::float32(0.2);
        highPointLight.diffuse =  glm::vec3(1, 1, 1) *  glm::float32(0.6);
        highPointLight.specular =  glm::vec3(1, 1, 1) * glm::float32(1);

//        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
//        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
//        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
//        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        material.ambient =  glm::vec3(1, 1, 1) * glm::float32(1.);
        material.diffuse =  glm::vec3(1, 1, 1) * glm::float32(0.9);
        material.specular =  glm::vec3(1    , 1, 1) * glm::float32(0.1) ;


        GLint uniformLoc;
        //Переводим положение ИС из мировой СК в СК виртуальной камеры
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(highPointLight.position, 1.0));
        uniformLoc = glGetUniformLocation(_shader->id(), "light.pos");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(lightPosCamSpace));
        uniformLoc = glGetUniformLocation(_shader->id(), "light.La");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(highPointLight.ambient));
        uniformLoc = glGetUniformLocation(_shader->id(), "light.Ld");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(highPointLight.diffuse));
        uniformLoc = glGetUniformLocation(_shader->id(), "light.Ls");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(highPointLight.specular));
        uniformLoc = glGetUniformLocation(_shader->id(), "light.a0");
        glUniform1f(uniformLoc, highPointLight.attenuation0);
        uniformLoc = glGetUniformLocation(_shader->id(), "light.a1");
        glUniform1f(uniformLoc, highPointLight.attenuation1);
        uniformLoc = glGetUniformLocation(_shader->id(), "light.a2");
        glUniform1f(uniformLoc, highPointLight.attenuation2);

        uniformLoc = glGetUniformLocation(_shader->id(), "material.Ka");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(material.ambient));
        uniformLoc = glGetUniformLocation(_shader->id(), "material.Kd");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(material.diffuse));
        uniformLoc = glGetUniformLocation(_shader->id(), "material.Ks");
        glUniform3fv(uniformLoc, 1, glm::value_ptr(material.specular));
        uniformLoc = glGetUniformLocation(_shader->id(), "material.s");
        glUniform1f(uniformLoc, material.shininess);


//        glBindTexture(GL_TEXTURE_2D, _depthTexId);
//        glBindSampler(1,  _depthSampler);
//        _shader->setIntUniform("shadowTex", 1);


        _shader->setMat4Uniform("lightViewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("lightProjectionMatrix", _camera.projMatrix);

        glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
        _shader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);


        GLuint textureUnitForDiffuseTex = 0;
        GLuint textureUnitForNormalTex = 2;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _wallTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _wallSampler);

            glBindTextureUnit(textureUnitForNormalTex, _wallNormTexture->texture());
            glBindSampler(textureUnitForNormalTex, _wallSampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _wallTexture->texture());
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _wallTexture->bind();

            glBindSampler(textureUnitForNormalTex, _wallNormTexture->texture());
            glActiveTexture(GL_TEXTURE0 + textureUnitForNormalTex);  //текстурный юнит 0
            _wallNormTexture->bind();
        }
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
        _shader->setIntUniform("normalTex",textureUnitForNormalTex);

        _shader->setMat4Uniform("modelMatrix", _labyrinth_walls->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_walls->modelMatrix()))));
        _shader->setIntUniform("material.haveNormalMap", 1);

        _shader->setFloatUniform("material.s", glm::float32(16.0));
        _labyrinth_walls->draw();


        GLuint textureUnitForDiffuseTex2 = 0;
        GLuint textureUnitForNormalTex2 = 2;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex2, _floorTexture->texture());
            glBindSampler(textureUnitForDiffuseTex2, _floorSampler);

            glBindTextureUnit(textureUnitForNormalTex2, _floorNormTexture->texture());
            glBindSampler(textureUnitForNormalTex2, _floorSampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex2, _floorTexture->texture());
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex2);  //текстурный юнит 0
            _floorTexture->bind();

            glBindSampler(textureUnitForNormalTex2, _floorNormTexture->texture());
            glActiveTexture(GL_TEXTURE0 + textureUnitForNormalTex2);  //текстурный юнит 0
            _floorNormTexture->bind();
        }
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex2);
        _shader->setIntUniform("normalTex",textureUnitForNormalTex2);

        material.ambient =  glm::vec3(1, 1, 1) * glm::float32(1.);
        material.diffuse =  glm::vec3(1, 1, 1) * glm::float32(0.8);
        material.specular =  glm::vec3(1    , 1, 1) * glm::float32(1.) ;

        _shader->setVec3Uniform("material.Ka", material.ambient);
        _shader->setVec3Uniform("material.Kd", material.diffuse);
        _shader->setVec3Uniform("material.Ks", material.specular);
        _shader->setFloatUniform("material.s", glm::float32(128.0));

        _shader->setMat4Uniform("modelMatrix", _labyrinth_floor->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_floor->modelMatrix()))));
        _shader->setIntUniform("material.haveNormalMap", 1);

        _labyrinth_floor->draw();


        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _highFloorTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _highFloorSampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _highFloorTexture->texture());
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0

            _highFloorTexture->bind();
        }
        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);

        _shader->setMat4Uniform("modelMatrix", _labyrinth_high_walls->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _labyrinth_high_walls->modelMatrix()))));
        _shader->setIntUniform("material.haveNormalMap", 0);

        _labyrinth_high_walls->draw();

        GLuint textureUnitForPlacats = 4;
        if (USE_DSA) {
            for (int i = 0; i < 3; i++) {
                glBindTextureUnit(textureUnitForPlacats+i, _placats_texts[i]->texture());
                glBindSampler(textureUnitForPlacats+i, _wallSampler);
            }
        } else {
            for (int i = 0; i < 3; i++) {
                glBindSampler(textureUnitForPlacats + i, _wallSampler);
                glActiveTexture(GL_TEXTURE0 + textureUnitForPlacats + i);  //текстурный юнит 4
                _placats_texts[i]->bind();
            }
        }

        material.ambient =  glm::vec3(1, 1, 1) * glm::float32(1.);
        material.diffuse =  glm::vec3(1, 1, 1) * glm::float32(0.7);
        material.specular =  glm::vec3(1    , 1, 1) * glm::float32(0.1);

        _shader->setVec3Uniform("material.Ka", material.ambient);
        _shader->setVec3Uniform("material.Kd", material.diffuse);
        _shader->setVec3Uniform("material.Ks", material.specular);
        _shader->setFloatUniform("material.s", material.shininess);
        _shader->setIntUniform("material.haveNormalMap", 0);

        for (int i = 0; i < 3; i++) {
            _shader->setIntUniform("diffuseTex", textureUnitForPlacats + i);
            _shader->setMat4Uniform("modelMatrix", _placats[i]->modelMatrix());
            _shader->setMat3Uniform(
                    "normalToCameraMatrix",
                    glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _placats[i]->modelMatrix())))
                    );
            _placats[i]->draw();
        }


        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex2);

        _shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _cube->modelMatrix()))));
        _cube->draw();
    }
};

int main()
{
    SampleApplication app;
    app._cameraMover = std::make_shared<FreeCameraMover>();
    app.start();

    return 0;
}