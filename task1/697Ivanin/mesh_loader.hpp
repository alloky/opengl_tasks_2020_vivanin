//
// Created by alloky on 09.03.2020.
//

#include <Mesh.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <string>


MeshPtr load_mesh(const std::string& filename) {
    std::ifstream f;
    f.open(filename);

    unsigned int v_cnt;
    f >> v_cnt;

    std::vector<glm::vec3> v(v_cnt);
    std::vector<glm::vec3> n(v_cnt);
    std::vector<glm::vec2> uv(v_cnt);
    std::vector<glm::vec3> t(v_cnt);
    std::vector<glm::vec3> b(v_cnt);

    float x, y, z;

    for (int i = 0; i < v_cnt; ++i)   {
        f >> x >> y >> z;
        v[i] = glm::vec3(x, y, z);
    }

    for (int i = 0; i < v_cnt; ++i)   {
        f >> x  >> y >> z;
        n[i] = glm::vec3(x, y, z);
    }

    for (int i = 0; i < v_cnt; ++i)   {
        f >> x >> y ;
        uv[i] = glm::vec2(x, y);
    }

    for (int i = 0; i < v_cnt; ++i) {
        f >> x >> y >> z;
        t[i] = glm::vec3(x, y, z);
        f >> x >> y >> z;
        b[i] = glm::vec3(x, y, z);
    }

    DataBufferPtr vbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr nbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr uvbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr tbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    DataBufferPtr bbuf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);

    vbuf->setData(v.size() * sizeof(float) * 3, v.data());
    nbuf->setData(n.size() * sizeof(float) * 3, n.data());
    uvbuf->setData(uv.size() * sizeof(float) * 2, uv.data());
    tbuf->setData(t.size() * sizeof(float) * 3, t.data());
    bbuf->setData(b.size() * sizeof(float) * 3, b.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vbuf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, nbuf);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, uvbuf);
    mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, tbuf);
    mesh->setAttribute(4, 3, GL_FLOAT, GL_FALSE, 0, 0, bbuf);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(v.size());

    return mesh;
}